from rest_framework import serializers
from .models import Seller


class SellerProflieUpdateInfosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Seller
        exclude = ['seller']

class SellerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Seller
        fields = ['id', 'seller', 'image', 'first_name', 'last_name',
                  'dob', 'company_name', 'company_address', 'opening_time', 'closing_time', 'city', 'country', 'postcode', 'status']




class SellerProflieAddInfosSerializer(serializers.ModelSerializer):

    first_name = serializers.CharField()
    last_name = serializers.CharField(required=True)
    company_name = serializers.CharField(required=True)
    company_address = serializers.CharField(required=True)
    city = serializers.CharField(required=True)
    country = serializers.CharField(required=True)
    postcode = serializers.CharField(required=True)
    opening_time = serializers.CharField(required=True)
    closing_time = serializers.CharField(required=True)



    class Meta:
        model = Seller
        fields = ['id','first_name', 'last_name', 'dob', 'company_name', 'company_address', 'opening_time',
                  'closing_time', 'city', 'country', 'postcode', 'status', 'image']