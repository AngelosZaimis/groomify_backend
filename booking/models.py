from django.db import models
from datetime import datetime
from django.contrib.auth import get_user_model

from seller_profile.models import Seller
from service.models import Service

User = get_user_model()


class Appointment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default="")
    service = models.ForeignKey(Service, on_delete=models.CASCADE, null=False, default="")
    seller = models.ForeignKey(Seller, on_delete=models.CASCADE, null=False, default="")
    date = models.DateField(default=datetime.now)
    start_time = models.TimeField(null=True, blank=True)
    end_time = models.TimeField(null=True, blank=True)
    confirmed = models.BooleanField(default=False)
    time_ordered = models.DateTimeField(default=datetime.now, blank=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['date', 'start_time', 'seller'], name='unique_appointment_time')
        ]

    def __str__(self):
        return f"{self.user.username} | day: {self.date} | time: {self.start_time}"
