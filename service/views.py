from django.http import HttpResponse
from django.views import View
from rest_framework import status
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, ListAPIView
from datetime import timedelta
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from service.models import Service
from .serializers import ServiceSerializer
from seller_profile.models import Seller

class ListCreateServiceView(ListCreateAPIView):
    queryset = Service.objects.all().order_by('created_time')
    serializer_class = ServiceSerializer


class RetrieveUpdateDeleteServiceView(RetrieveUpdateDestroyAPIView):
    queryset = Service.objects.all()
    serializer_class = ServiceSerializer


class ListUserServicesView(ListAPIView):
    queryset = Service.objects.all()
    serializer_class = ServiceSerializer

    def get_queryset(self):
        user = self.request.user
        return Service.objects.filter(seller_profile__seller=user).order_by('created_time')


class CreateServiceView(APIView):

    def post(self, request):
        service = request.data.get('service')
        description = request.data.get('description', '')
        price = request.data.get('price', '')
        duration = request.data.get('duration', '')
        seller_profile = Seller.objects.get(seller=request.user)

        duration_in_hours = int(duration)

        service_obj = Service.objects.create(
            service=service,
            description=description,
            price=price,
            duration_in_hours=duration_in_hours,
            seller_profile=seller_profile
        )

        # Do something with the new Service object

        return HttpResponse(status=201)
