from django.urls import path

from .views import ListCreateAppointmentView, RetrieveUpdateDeleteAppointmentView, ListUserAppointmentView

urlpatterns = [
    path('', ListCreateAppointmentView.as_view()),
    path('<int:pk>/', RetrieveUpdateDeleteAppointmentView.as_view()),
    path('me/', ListUserAppointmentView.as_view()),
]