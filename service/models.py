import datetime

from seller_profile.models import Seller
from django.db import models



SERVICE_CHOICES = (
    ("Washing", "Washing"),
    ("Cutting and washing", "Cutting and washing"),
    ("Dental care", "Dental care"),
    ("Nail cutting", "Nail cutting"),
    ("Full service", "Full service"),
)


class Service(models.Model):

    service = models.CharField(max_length=50, choices=SERVICE_CHOICES, default="Full service")
    description = models.TextField(max_length=200, blank=True)
    price = models.DecimalField(max_digits=5, decimal_places=2, null=False)
    duration_in_hours = models.IntegerField(default=1)
    seller_profile = models.ForeignKey(to=Seller, on_delete=models.CASCADE, related_name='services')
    created_time = models.DateTimeField(auto_now_add=True)
    updated_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'Service: {self.pk} - {self.service}'

