from rest_framework import serializers
from .models import Appointment


class AppointmentSerializer(serializers.ModelSerializer):
    service = serializers.ReadOnlyField(source="service.service")
    user = serializers.ReadOnlyField(source="user.email")
    seller = serializers.ReadOnlyField(source="seller.seller.email")

    class Meta:
        model = Appointment
        fields = ['id', 'user', 'service', 'seller', 'date', 'start_time', 'end_time', 'confirmed', 'time_ordered']
