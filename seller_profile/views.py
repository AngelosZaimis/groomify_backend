from django.shortcuts import render
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, ListAPIView
from seller_profile.serializers import SellerSerializer, SellerProflieAddInfosSerializer
from seller_profile.models import Seller
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from seller_profile.serializers import SellerProflieUpdateInfosSerializer
class SellerView(ListCreateAPIView):
    queryset = Seller.objects.all()
    serializer_class = SellerSerializer


class UserDetailView(APIView):
    def get(self, request):
        if not request.user.is_authenticated:
            return Response({'detail': 'Authentication credentials were not provided.'},
                            status=status.HTTP_401_UNAUTHORIZED)

        try:
            seller = Seller.objects.get(seller=request.user)
        except Seller.DoesNotExist:
            seller = None

        data = {
            'id': request.user.id,
            'username': request.user.username,
            'email': request.user.email,
            'first_name': seller.first_name,
            'last_name': seller.last_name,
            'city': seller.city if seller else '',
            'company_address': seller.company_address if seller else '',
            'company_name': seller.company_name if seller else '',
            'country': seller.country if seller else '',
            'dob': seller.dob if seller else '',
            'opening_time': seller.opening_time if seller else '',
            'closing_time': seller.closing_time if seller else '',
            'post_code': seller.postcode if seller else '',
            'image': request.build_absolute_uri(seller.image.url) if seller and seller.image else None,

            # add any additional user information fields here
        }
        return Response(data)


class SellerProfileInformations(generics.GenericAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = SellerProflieAddInfosSerializer

    def post(self, request):
        user = request.user
        seller_profile = Seller.objects.get(seller=user)
        serializer = self.serializer_class(seller_profile, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({'message': 'Informations succesfully added!'})

    def patch(self, request):
        user = request.user
        seller_profile = Seller.objects.get(seller=user)
        serializer = SellerProflieUpdateInfosSerializer(seller_profile, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({'message': 'Informations succesfully updated!'})




