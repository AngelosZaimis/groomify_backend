# Generated by Django 4.1.6 on 2023-02-17 20:38

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Appointment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(default=datetime.datetime.now)),
                ('start_time', models.TimeField(blank=True, null=True)),
                ('end_time', models.TimeField(blank=True, null=True)),
                ('confirmed', models.BooleanField(default=False)),
                ('time_ordered', models.DateTimeField(blank=True, default=datetime.datetime.now)),
            ],
        ),
    ]
