from django.db import models

# Create your models here.
from django.db import models
from custom_user.models import User
from booking.models import Appointment

PET_SIZE_CHOICES = (
    ("Small", "Small"),
    ("Medium", "Medium"),
    ("Large", "Large"),
)


class PetProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=20, default='')
    last_name = models.CharField(max_length=20, default='')
    your_pet = models.CharField(max_length=200, default='')
    pet_name = models.CharField(max_length=20, default='')
    pet_size = models.CharField(
        max_length=10, choices=PET_SIZE_CHOICES, default="Medium")
    pet_photo = models.ImageField(null=True, blank=True, upload_to='images/')
    description = models.TextField(max_length=200, default='')
    your_appointments = models.OneToOneField(
        to=Appointment, on_delete=models.CASCADE, blank=True, null=True)


class Meta:
    model = PetProfile
    fields = ( 'first_name', 'last_name', 'your_pet', 'pet_name',
              'pet_size', 'pet_photo', 'description', 'your_appointments')


def __str__(self):
    return f'{self.your_pet} {self.pet_name} profile'
