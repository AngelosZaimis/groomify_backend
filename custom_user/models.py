import random

from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.


def generate_code(length=5):
    numbers = '0123456789'
    return ''.join(random.choice(numbers) for _ in range(length))


class User(AbstractUser):

    # default values for verification and activation
    is_verified = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    first_login = models.BooleanField(default=True)

    user_type = models.CharField(max_length=20, null=True)
    # code for verification and validation
    code = models.IntegerField(default=generate_code)

    USERNAME_FIELD = 'email'

    REQUIRED_FIELDS = ['username']

    email = models.EmailField(unique=True)

    def __str__(self):
        return f"ID: {self.id} email: {self.email}"