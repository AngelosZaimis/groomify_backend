from django.db import models
from django.contrib.auth import get_user_model
from django.core.validators import MinValueValidator, MaxValueValidator
from seller_profile.models import Seller
from pet_profile.models import PetProfile

User = get_user_model()


def comment_directory_path(instance, filename):
    return f'{instance.author.id}/{filename}'


class Comment(models.Model):
    text_content = models.TextField(max_length=200, blank=False, null=False)
    images = models.ImageField(upload_to=comment_directory_path, blank=True, null=True)
    rating = models.IntegerField(default=3,
                                 validators=[MinValueValidator(1), MaxValueValidator(5)])
    author = models.ForeignKey(to=User, on_delete=models.CASCADE)
    seller_profile = models.ForeignKey(to=Seller, on_delete=models.CASCADE, blank=True, null=True)
    pet_profile = models.ForeignKey(to=PetProfile, on_delete=models.CASCADE, blank=True, null=True)
    created_time = models.DateTimeField(auto_now_add=True)
    updated_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'Comment: {self.pk} from ({self.author}) - {self.text_content}'
