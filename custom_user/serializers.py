from rest_framework import serializers
from django.contrib.auth import get_user_model


User = get_user_model() # get the custom user model

class RegisterSerializer(serializers.ModelSerializer):

    password = serializers.CharField(min_length=6, max_length=50, write_only=True) # validate password
    user_type = serializers.CharField(required=True)
    class Meta:
        model = User
        fields = ['email', 'username', 'password','user_type']

    def validate(self, attrs):
        username = attrs.get('username', '')
        password = attrs.get('password', '')


        if not username.isalnum():
            raise  serializers.ValidationError("The username should only contain alphanumeric charachters.")

        if not len(password) >= 6:
            raise serializers.ValidationError("The password should be 6 digits or more and less than 50")


        return attrs
    def create(self, validated_data): # we create the user
        return User.objects.create_user(**validated_data)




class ChangePasswordSerializer(serializers.Serializer):
    email = serializers.EmailField()

    def validate_email(self, value):
        try:
            User.objects.get(email=value)
        except User.DoesNotExist:
            raise serializers.ValidationError("User with this email does not exist")
        return value

class ChangePasswordVerifySerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField(write_only=True)
    code = serializers.CharField(max_length=555)

    def validate(self, data):
        email = data.get('email')
        code = data.get('code')
        try:
            user = User.objects.get(email=email, code=code)
        except User.DoesNotExist:
            raise serializers.ValidationError("User with given email and code does not exist.")

        return data


class EmailVerificationSerializer(serializers.ModelSerializer):
    code = serializers.CharField(max_length=555)
    class Meta:
        model = User
        fields = ['code']


