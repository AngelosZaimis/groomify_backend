from django.urls import path
from .views import ListCreateCommentView, RetrieveUpdateDeleteCommentView, ListUserCommentsView

urlpatterns = [
    path('', ListCreateCommentView.as_view()),
    path('<int:pk>/', RetrieveUpdateDeleteCommentView.as_view()),
    path('me/', ListUserCommentsView.as_view()),
]
