from rest_framework import serializers
from .models import PetProfile, PET_SIZE_CHOICES



class PetProflieUpdateInfosSerializer(serializers.ModelSerializer):
    class Meta:
        model = PetProfile
        exclude = ['user']

class PetProfileSerializer(serializers.ModelSerializer):



    class Meta:
        model = PetProfile
        fields = ['id', 'first_name', 'last_name',
                  'your_pet', 'pet_name', 'pet_size', 'pet_photo', 'description', 'your_appointments']




class PetProfileAddInfosSerializer(serializers.ModelSerializer):

    first_name = serializers.CharField()
    last_name = serializers.CharField()
    your_pet = serializers.CharField(max_length=200)
    pet_name = serializers.CharField()
    description = serializers.CharField()


    class Meta:
        model = PetProfile
        fields = ['id', 'first_name', 'last_name',
                  'your_pet', 'pet_name', 'pet_size', 'pet_photo', 'description', 'your_appointments']