from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, ListAPIView
from pet_profile.serializers import PetProfileSerializer, PetProfileAddInfosSerializer
from pet_profile.models import PetProfile
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView
from pet_profile.serializers import PetProflieUpdateInfosSerializer

class PetProfileView(ListCreateAPIView):
    queryset = PetProfile.objects.all()
    serializer_class = PetProfileSerializer


# for getting the information  of pet_profile user
class UserDetailView(APIView):
    def get(self, request):
        if not request.user.is_authenticated:
            return Response({'detail': 'Authentication credentials were not provided.'},
                            status=status.HTTP_401_UNAUTHORIZED)

        user = request.user
        data = {
            'id': user.id,
            'username': user.username,
            'email': user.email,
            'first_name': user.petprofile.first_name,
            'last_name' : user.petprofile.last_name,
            'your_pet': user.petprofile.your_pet,
            'pet_name': user.petprofile.pet_name,
            'pet_size': user.petprofile.pet_size,
            'description': user.petprofile.description,
            'your_appointments': user.petprofile.your_appointments,
            'pet_photo': request.build_absolute_uri(user.petprofile.pet_photo.url) if user.petprofile and user.petprofile.pet_photo else None,
            # add any additional user information fields here
        }
        return Response(data)

class PetProfileInformations(generics.GenericAPIView):

    serializer_class = PetProfileAddInfosSerializer


    def post(self,request):
        user = request.user
        pet_profile = PetProfile.objects.get(user=user)
        serializer = self.serializer_class(pet_profile, data=request.data,)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({'message': 'Informations succesfully added'})



    def patch(self,request):
        user = request.user
        pet_profile = PetProfile.objects.get(user=user)
        serializer = PetProflieUpdateInfosSerializer(pet_profile, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({'message': 'Informations succesfully updated'})