# Generated by Django 4.1.6 on 2023-02-18 09:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('seller_profile', '0001_initial'),
        ('service', '0005_alter_service_seller_profile'),
    ]

    operations = [
        migrations.AlterField(
            model_name='service',
            name='seller_profile',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Services', to='seller_profile.seller'),
        ),
    ]
