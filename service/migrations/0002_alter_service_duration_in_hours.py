# Generated by Django 4.1.6 on 2023-02-17 21:01

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='service',
            name='duration_in_hours',
            field=models.DurationField(default=datetime.timedelta(seconds=3600)),
        ),
    ]
