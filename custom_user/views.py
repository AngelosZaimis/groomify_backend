
from rest_framework import generics, status
from rest_framework.response import Response
from django.contrib.auth import get_user_model
from .serializers import RegisterSerializer, EmailVerificationSerializer, ChangePasswordSerializer, ChangePasswordVerifySerializer
from .utils import Util
from rest_framework.views import APIView
from rest_framework_simplejwt.views import TokenObtainPairView
from seller_profile.models import Seller
from pet_profile.models import PetProfile


# Create your views here.
User = get_user_model()

class RegisterView(generics.GenericAPIView):

    serializer_class = RegisterSerializer

    def post(self , request):
        user = request.data # data that we get from user
        serializer = self.serializer_class(data=user) # we call the serializer and pass the data for verification
        serializer.is_valid(raise_exception=True) # validation
        serializer.save() # save new user if everything is ok

        user_data = serializer.data # we created the user and from this variable we get his data


        user = User.objects.get(email=user_data['email'])
        # The email that user will get


        email_body = 'Welcome to Groomify , ' + user.username.capitalize() + '!' + '\n' + 'Copy the Code to verify your email. \n' + 'CODE:'+ str(user.code)

        # the data
        data = {
            'email_body' : email_body,
            'to_email': user.email,
            'email_subject': 'Verify your email'
        }

        Util.send_email(data)

        user_type = request.data.get('user_type')


        if user_type == 'seller_profile':
            seller_profile = Seller.objects.create(seller=user)
            seller_profile.save()
        elif user_type == 'pet_profile':
            pet_profile = PetProfile.objects.create(user=user)
            pet_profile.save()


        return Response({'message': 'You have been registered, check your emails for the validation code', 'data': user_data},
                        status=status.HTTP_201_CREATED)



# change email first request
class ChangePasswordView(generics.GenericAPIView):
    serializer_class = ChangePasswordSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        user_email = serializer.validated_data['email']
        try:
            user = User.objects.get(email=user_email)
        except User.DoesNotExist:
            return Response({'error': 'User with this email does not exist'}, status=status.HTTP_404_NOT_FOUND)

        email_body = 'Welcome back ' + user.username.capitalize() + '!' + ' Did you forget your password? No worries \n Copy this code to verify and change your password \n' + 'CODE: ' + str(
            user.code)
        data = {
            'email_body': email_body,
            'to_email': user.email,
            'email_subject': 'Change password'
        }

        Util.send_email(data)
        return Response(
            {'message': 'Check your emails, you have received a code to change your password.', 'email': user.email},
            status=status.HTTP_201_CREATED)



class ChangePasswordVerify(APIView):
    serializer_class = ChangePasswordVerifySerializer

    def patch(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        email = validated_data.get('email')
        code = validated_data.get('code')
        password = validated_data.get('password')

        user = User.objects.get(email=email, code=code)
        user.set_password(password)
        user.save()

        return Response({"message": "Password changed successfully."}, status=status.HTTP_200_OK)


class VerifyEmail(APIView):

    serializer_class = EmailVerificationSerializer


    def post(self, request):

        serializer = self.serializer_class(data=request.data) # get users data
        serializer.is_valid(raise_exception=True)
        code = serializer.validated_data.get('code')

        try :

            user = User.objects.get(code=code) # if code from reqyest and code in user model is same
            user.is_verified = True #set user as verified
            user.save() #and save it
            return Response({'email': 'Successfully verified, thank you very much'},status=status.HTTP_200_OK) # success
        except User.DoesNotExist:
            return Response({'error': 'Invalid verification code, please try again.'})



# custom get token view
class CustomTokenObtainPairView(TokenObtainPairView):
    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        username = request.data.get('email')
        user = User.objects.get(email=username)

        is_first_login = user.first_login

        if is_first_login:
            # Update the user's first_login field in the database
            user.first_login = False
            user.save()

        data = {
            'message': 'Successful login.',
            'username': user.username,
            'user_type': user.user_type,
            'token': response.data['access'],
            'first_login': is_first_login
        }
        return Response(data, status=status.HTTP_200_OK)