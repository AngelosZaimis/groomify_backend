from django.db import models
from django.contrib.auth import get_user_model
import datetime


User = get_user_model()


class Seller(models.Model):  # seller details
    seller = models.ForeignKey(User, on_delete=models.CASCADE, related_name="Seller",)  # user foreign key
    image = models.ImageField(default="default.png", upload_to="profile_pictures", null=True,
                              blank=True)  # profile picture
    first_name = models.CharField(
        max_length=100, default='')  # seller first name
    last_name = models.CharField(
        max_length=100, default='')  # seller last name
    dob = models.DateField(default=datetime.date.today)  # seller date of birth
    company_name = models.CharField(
        max_length=300, default='')  # seller address
    company_address = models.CharField(
        max_length=300, default='')  # seller address
    opening_time = models.TimeField(default=datetime.time(
        hour=9, minute=0))  # store opening time
    closing_time = models.TimeField(default=datetime.time(
        hour=17, minute=0))  # store closing time
    city = models.CharField(max_length=100, default='')  # seller city
    country = models.CharField(max_length=100, default='')  # seller country
    postcode = models.CharField(max_length=4, default=0)  # seller postcode
    # seller status (approved/on-hold)
    status = models.BooleanField(default=False)

    class Meta:
        app_label = 'seller_profile'

    def __str__(self):
        return f'{self.seller.username} Seller Profile'



