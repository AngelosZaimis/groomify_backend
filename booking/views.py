import datetime

from django.contrib.auth import get_user_model
from rest_framework.generics import RetrieveUpdateDestroyAPIView, ListCreateAPIView, ListAPIView
from rest_framework.views import APIView

from custom_user.utils import Util
from seller_profile.models import Seller
from .serializers import AppointmentSerializer
from django.http import JsonResponse
from django.core.mail import send_mail
from .models import Appointment
from service.models import Service


User = get_user_model()


class ListCreateAppointmentView(ListCreateAPIView):
    queryset = Appointment.objects.all()
    serializer_class = AppointmentSerializer

    def post(self, request, *args, **kwargs):
        service_id = request.data.get('service')
        date = request.data.get('date')
        start_time_str = request.data.get('start_time')
        user = request.data.get('user')

        # Get the selected service and User
        selected_service = Service.objects.get(id=service_id)

        # Calculate end_time
        duration_in_hours = selected_service.duration_in_hours
        start_time_arr = start_time_str.split(':')
        start_time_int = int(''.join(start_time_arr))
        start_time_arr[0] = int(float(start_time_arr[0]) + duration_in_hours)
        end_time_str = ':'.join(map(str, start_time_arr))

        # Calculate end_time_int
        end_time_arr = end_time_str.split(':')
        end_time_int = int(''.join(end_time_arr))

        # Get the seller associated with the appointment
        seller_id = request.data.get('seller')
        seller = Seller.objects.get(id=seller_id)

        # Convert opening_time to int
        # Convert opening and closing times to datetime objects
        opening_time_arr = str(seller.opening_time).split(':')
        opening_time_str = ''.join(map(str, opening_time_arr))
        opening_time = datetime.datetime.strptime(opening_time_str, '%H%M%S').time()

        closing_time_arr = str(seller.closing_time).split(':')
        closing_time_str = ''.join(map(str, closing_time_arr))
        closing_time = datetime.datetime.strptime(closing_time_str, '%H%M%S').time()

        # Convert start and end times to datetime objects
        start_time = datetime.datetime.strptime(start_time_str, '%H:%M:%S').time()
        end_time = datetime.datetime.strptime(end_time_str, '%H:%M:%S').time()

        # Check if the appointment falls within the seller's opening hours
        if start_time < opening_time or end_time > closing_time:
            return JsonResponse({'success': False, 'error': 'Appointment outside of opening hours'})

        appointment = Appointment.objects.create(
            user=user,
            service=selected_service,
            seller=seller,
            date=date,
            start_time=start_time_str,
            end_time=end_time_str,
        )

        email_body = f'Appointment Booked, Your appointment for {appointment.service.service} on {appointment.date} from {appointment.start_time} to {appointment.end_time} has been booked successfully.'

        # the data
        data = {
            'email_body': email_body,
            'to_email': request.user.email,
            'email_subject': 'Appointment confirmation'
        }

        Util.send_email(data)

        return JsonResponse({'success': True})
class AvailableAppointmentView(APIView):
    def get(self, request, service_id, seller_id):
        # Get the selected service and seller
        selected_service = Service.objects.get(id=service_id)
        seller = Seller.objects.get(id=seller_id)

        # Get the existing appointments for the selected service and seller
        appointments = Appointment.objects.filter(service=selected_service, seller=seller)

        # Get the seller's opening hours
        opening_time = seller.opening_time
        closing_time = seller.closing_time

        # Get the available dates and times based on the existing appointments and the opening hours
        available_dates = []
        available_times = []

        # Loop over the next 7 days
        for i in range(7):
            # Get the date for the current day
            date = timezone.now().date() + timedelta(days=i)

            # Check if the date falls within the seller's opening hours
            if not (date.weekday() == 5 or date.weekday() == 6 or date in seller.holidays.all()):
                start_time = datetime.combine(date, opening_time)
                end_time = datetime.combine(date, closing_time)

                # Loop over all the appointments for the current date
                for appointment in appointments.filter(date=date):
                    appointment_start_time = datetime.combine(date, appointment.start_time)
                    appointment_end_time = datetime.combine(date, appointment.end_time)

                    # Remove the time slot of the current appointment from the available time slots
                    if appointment_start_time <= start_time <= appointment_end_time:
                        start_time = appointment_end_time
                    if appointment_start_time <= end_time <= appointment_end_time:
                        end_time = appointment_start_time

                # Add the remaining time slots to the available time slots
                while start_time < end_time:
                    available_dates.append(date)
                    available_times.append(start_time.time())
                    start_time += selected_service.duration_in_hours

        # Serialize the available dates and times
        data = {
            'dates': [date.strftime('%Y-%m-%d') for date in available_dates],
            'times': [time.strftime('%H:%M') for time in available_times],
        }
        return Response(data)


class RetrieveUpdateDeleteAppointmentView(RetrieveUpdateDestroyAPIView):
    queryset = Appointment.objects.all()
    serializer_class = AppointmentSerializer


class ListUserAppointmentView(ListAPIView):
    queryset = Appointment.objects.all()
    serializer_class = AppointmentSerializer

    def get_queryset(self):
        user = self.request.user
        return Appointment.objects.filter(seller__seller=user)


