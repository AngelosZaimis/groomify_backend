from rest_framework import serializers
from .models import Comment

class CommentSerializer(serializers.ModelSerializer):
    author = serializers.ReadOnlyField(source='author.email')
    seller_profile = serializers.ReadOnlyField(source='seller_profile.seller.email')
    pet_profile = serializers.ReadOnlyField(source='pet_profile.user.email')
    class Meta:
        model = Comment
        fields = ('id', 'text_content', 'author', 'seller_profile', 'pet_profile', 'rating', 'created_time', 'updated_time')
