from django.urls import path
from .views import PetProfileView,PetProfileInformations, UserDetailView


urlpatterns = [
    path('', PetProfileView.as_view()),
    path('infos/', UserDetailView.as_view()),
    path('add-informations/', PetProfileInformations.as_view())
]
