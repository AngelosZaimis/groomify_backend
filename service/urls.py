from django.urls import path
from service.views import ListCreateServiceView, RetrieveUpdateDeleteServiceView, ListUserServicesView, CreateServiceView

urlpatterns = [
    path('all/', ListCreateServiceView.as_view()),
    path('<int:pk>/', RetrieveUpdateDeleteServiceView.as_view()),
    path('me/', ListUserServicesView.as_view()),
    path('', CreateServiceView.as_view()),
]
