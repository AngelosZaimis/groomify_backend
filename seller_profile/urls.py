from django.urls import path
from .views import SellerView, SellerProfileInformations, UserDetailView

urlpatterns = [
    path('', SellerView.as_view()),
    path('infos/', UserDetailView.as_view()),
    path('add-informations/', SellerProfileInformations.as_view())
]
