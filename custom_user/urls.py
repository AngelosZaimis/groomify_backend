from django.urls import path

from .views import RegisterView, VerifyEmail, ChangePasswordView, ChangePasswordVerify


urlpatterns = [
    path('registration/', RegisterView.as_view(), name="register"),
    path('registration/validation/', VerifyEmail.as_view(), name="email-validation"),
    path('password-reset/', ChangePasswordView.as_view(), name="change-password"),
    path('password-reset/validation/', ChangePasswordVerify.as_view(), name="password-validation")

]
