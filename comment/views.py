from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, ListAPIView
from rest_framework import filters
from comment.serializers import CommentSerializer
from comment.models import Comment


class ListCreateCommentView(ListCreateAPIView):
    queryset = Comment.objects.all().order_by('created_time')
    serializer_class = CommentSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['text_content', 'rating']

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)


class RetrieveUpdateDeleteCommentView(RetrieveUpdateDestroyAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer


class ListUserCommentsView(ListAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    def get_queryset(self):
        user = self.request.user
        return Comment.objects.filter(author=user).order_by('created_time')
