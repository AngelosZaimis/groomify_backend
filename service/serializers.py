from rest_framework import serializers
from .models import Service

class ServiceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Service
        fields = ('id', 'service', 'description', 'price', 'duration_in_hours', 'seller_profile', 'created_time', 'updated_time')
